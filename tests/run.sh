#!/bin/bash

REFERENCE=`which bash`
TEST_SUBJECT=`which bash`

# Make sure it can be run from anywhere and still work
DIR=$(dirname "$(readlink -f "$0")")

cd $DIR
rm -rf tmp
mkdir tmp

function ok_message {
	tput setaf 2;
	echo "✓ Test passed at $1"
	tput sgr0;
}

function fail_message {
	tput setaf 1;
	echo "✗ Test failed at $1:"
	tput sgr0;
	diff $2 $3
}

let SUCCESS_COUNT=0
let FAIL_COUNT=0

# function that runs a test
function run_a_test {
	INPUT="$1.input" 
	if [ -f $INPUT ]; then
		$REFERENCE $1 < $INPUT > tmp/reference.output
		$TEST_SUBJECT $1 < $INPUT > tmp/test_subject.output
	else
		$REFERENCE $1 > tmp/reference.output
		$TEST_SUBJECT $1 > tmp/test_subject.output
	fi
	REFERENCE_HASH=`md5sum < tmp/reference.output`
	TEST_HASH=`md5sum < tmp/test_subject.output`
	if [ "$REFERENCE_HASH" != "$TEST_HASH" ]; then
		let FAIL_COUNT++
		fail_message $1 tmp/reference.output tmp/test_subject.output
	else
		let SUCCESS_COUNT++
		ok_message $1
	fi
}

if [ $1 ]; then
	run_a_test $1
else
	for f in ./*; do
		case "$f" in
			./run.sh) ;; # ignore this file
			./02-random.sh) ;; # this test should always fail, useful for testing this script
			*.sh) run_a_test $f
		esac
	done
	if [ 0 != $FAIL_COUNT ]; then
		echo "Errors encountered ($FAIL_COUNT)"
		exit 1
	else
		echo "All ($SUCCESS_COUNT) tests successful"
		rm -rf tmp
		exit 0
	fi
fi
